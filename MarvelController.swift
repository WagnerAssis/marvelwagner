//
//  MarvelController.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/18/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import SwiftyJSON
import Alamofire

class MarvelController : NSObject {
    
    //MARK: Routes
    
    let CHARACTERS_METHOD = "/v1/public/characters?"
    
    // MARK: Defines
    let connectionMarvel : ConnectionMarvelAPI!
    
    // MARK: Handler
    typealias handler = (Any,Bool) -> Void
    
    override init() {
        connectionMarvel = nil
    }
    
    init(main:UIViewController) {
        connectionMarvel = ConnectionMarvelAPI(main: main)
    }
    
    
    func GETAllCharactersMarvel(callback:@escaping handler){
        connectionMarvel.requestGET(methodAPI: CHARACTERS_METHOD, parameters: nil) { (result:Any, status:Bool) in
            
            if status{
                
            let resultJSON = JSON(data: (result as! DataResponse<Any>).data!)
            let marvelObject = Mapper<ResponseMarvel>().map(JSONString: resultJSON.description)
            
                callback(marvelObject!, true)
            }
            else{
                Alert.show(titulo: "Ops", mensagem: "Tente novamente mais tarde!", viewController: self.connectionMarvel.viewControllerUsed)
               callback("", false)
            }
            //Stop Loader
            self.connectionMarvel.loader.hideProgressHUD(animated: true)
            
        }
    }
}
