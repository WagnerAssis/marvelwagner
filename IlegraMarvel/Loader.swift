//
//  Loader.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/18/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import M13ProgressSuite

class Loader : NSObject {
    
    var progress : M13ProgressHUD!
    var controller:UIViewController!
    
    override init() {
        super.init()
    }
    
    init(main:UIViewController) {
        super.init()
        controller = main
        self.progressHUD()
    }
    
    // MARK - Progress
    func progressHUD()
    {
        let progressView = M13ProgressViewSegmentedRing()
        progressView.primaryColor = ColorsApp().corSecundaria
        progressView.secondaryColor = ColorsApp().corPrimaria
        progressView.progressRingWidth = 6.0
        progressView.numberOfSegments = 6
        progressView.showPercentage = false
        progress = M13ProgressHUD(progressView: progressView)
        progress.hudBackgroundColor = UIColor.clear
        progress.progressViewSize = CGSize(width:80.0,height: 80.0)
        progress.maskType  = M13ProgressHUDMaskTypeNone
        progress.minimumSize = CGSize(width:25.0,height: 25.0)
    
    }
    
    func showProgressHUDWithMessage(title:String)
    {
        controller.view.addSubview(self.progress)
        self.progress.status = title
        self.progress.statusFont = Fonts().fonteLoader
        self.progress.indeterminate = true
        self.progress.show(true)
        
        controller.navigationController?.navigationBar.isUserInteractionEnabled = false
    }
    
    func showProgressHUDWithMessage()
    {
        controller.view.addSubview(self.progress)
        self.progress.indeterminate = true
        self.progress.show(true)
        
        controller.navigationController?.navigationBar.isUserInteractionEnabled = false
    }
    
    func hideProgressHUD(animated:Bool)
    {
        self.progress.removeFromSuperview()
        self.progress.hide(animated)
        controller.navigationController?.navigationBar.isUserInteractionEnabled = true
    }
}
