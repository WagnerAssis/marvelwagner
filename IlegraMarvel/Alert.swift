//
//  Alert.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/18/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import SwiftyJSON

class Alert {
    
    typealias handler = (_ :Bool?) -> Void
    
    
    static func show(titulo:String, mensagem: String, viewController: UIViewController){
        
        
        let alert = UIAlertController(title: titulo, message: mensagem, preferredStyle: UIAlertControllerStyle.alert)
        alert.view.tintColor = ColorsApp().corPrimaria
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (alertAction) -> Void in
        }))
        
        viewController.present(alert, animated: true, completion: nil)
        
    }
    
    static func show(titulo:String, mensagem: String, viewController: UIViewController, callback: @escaping handler){
        
        let alert = UIAlertController(title: titulo, message: mensagem, preferredStyle: UIAlertControllerStyle.alert)
        alert.view.tintColor = ColorsApp().corPrimaria
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler:{ (alertAction) -> Void in
            callback(false)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (alertAction) -> Void in
            callback(true)
        
        }))
        
        
        
        viewController.present(alert, animated: true, completion: nil)
        
    }
    
    static func showJustWithOk(titulo:String, mensagem: String, viewController: UIViewController, callback: @escaping handler){
        
        let alert = UIAlertController(title: titulo, message: mensagem, preferredStyle: UIAlertControllerStyle.alert)
        alert.view.tintColor = ColorsApp().corPrimaria
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (alertAction) -> Void in
            callback(true)
            
        }))
        
        
        
        viewController.present(alert, animated: true, completion: nil)
        
    }
    
    static func showWithYesAndNo(titulo:String, mensagem: String, viewController: UIViewController, callback: @escaping handler){
        
        let alert = UIAlertController(title: titulo, message: mensagem, preferredStyle: UIAlertControllerStyle.alert)
        alert.view.tintColor = ColorsApp().corPrimaria
        alert.addAction(UIAlertAction(title: "Não", style: UIAlertActionStyle.cancel, handler:{ (alertAction) -> Void in
            callback(false)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Sim", style: UIAlertActionStyle.default, handler:{ (alertAction) -> Void in
            callback(true)
            
        }))
        
        
        
        viewController.present(alert, animated: true, completion: nil)
        
    }

    static func showErrorApplication(obj:AnyObject,viewcontroller:UIViewController)
    {
        show(titulo: "Ops", mensagem: obj.description ,viewController:viewcontroller)
    }
}
