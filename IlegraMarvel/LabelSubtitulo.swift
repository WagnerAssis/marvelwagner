//
//  LabelTitulo.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/19/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class LabelSubtitulo: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func setup() {
        
        font = Fonts().fonteSubtitulo
        textColor = ColorsApp().corSubtitulo
        
    }
    
    
}
