//
//  Cores.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/18/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import UIKit
import SwiftHEXColors

class ColorsApp {
    
    let corPrimaria = UIColor(hexString:"#a33e3e")
    let corSecundaria = UIColor(hexString: "606063")
    let corTitulo = UIColor(hexString: "000")
    let corSubtitulo = UIColor(hexString: "4D4D50")
    let corBotaoTexto = UIColor(hexString: "FFFFFF")
    let corBranca = UIColor(hexString:"fff")

}
