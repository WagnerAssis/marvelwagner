//
//  CharacterMarvel.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/19/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import ObjectMapper

class Item: Mappable{
    var name : String?

    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        
    }
}
