//
//  CharacterMarvel.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/19/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import ObjectMapper

class CharacterMarvel: Mappable{
    var idMarvel : Int?
    var name : String?
    var description : String?
    var thumbnail : Thumbnail?
    var urls : [Urls]?
    var comics : Comics?
    var stories : Stories?
    var events : Events?
    var series : Series?
    
    
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        idMarvel <- map["id"]
        name <- map["name"]
        description <- map["description"]
        thumbnail <- map["thumbnail"]
        urls <- map["urls"]
        comics <- map["comics"]
        stories <- map["stories"]
        events <- map["events"]
        series <- map["series"]
    }
}

extension CharacterMarvel: CardPresentable {
    
    var imageURLString: String {
        let urlImage = "\(thumbnail!.path!).\(thumbnail!.extensionThumb!)"
        return urlImage
    }
    
    var placeholderImage: UIImage? {
        return UIImage(named: "default")
    }
    
    var titleText: String {
        return name!
    }
    
    // This is used to tell the dial at the bottom of the UI which letter to point tofor this card
    var dialLabel: String {
        guard let lastString = titleText.components(separatedBy: "").last else { return "" }
        return String(lastString[lastString.startIndex])
    }
    
    var detailTextLineOne: String {
        return "Comics Available: \(comics!.available!)"
    }
    
    var detailTextLineTwo: String {
        return description!
    }
    
    var actionOne: CardAction? {
        return CardAction(title: "Info")
    }
    
    var actionTwo: CardAction? {
        return CardAction(title: "Bio")
    }
    
}
