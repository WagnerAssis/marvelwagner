//
//  ConnectionMarvelAPI.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/18/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import Alamofire
import M13ProgressSuite
import SwiftyJSON

class ConnectionMarvelAPI {
    
    // MARK: Defines
    
    let URL_API_Marvel = "https://gateway.marvel.com"
    let API_PUBLIC_KEY = "72e4c2dce7655ef289addbb756c8803d"
    let API_PRIVATE_KEY = "9a726f998978b7d144d78deb6b2ee646db191f0a"
    let currentTimeMillis = NSDate().timeIntervalSince1970.description
    let viewControllerUsed:UIViewController!
    var hashMD5:String!
    
    //Loader
    let loader:Loader!
    
    
    // MARK: Handler
    typealias handler = (Any,Bool) -> Void
    
    
    required init?(main:UIViewController) {
        hashMD5 = "\(currentTimeMillis)\(API_PRIVATE_KEY)\(API_PUBLIC_KEY)".toMd5()! as String
        viewControllerUsed = main
        loader = Loader(main: main)
    }
    
    init() {
        hashMD5 = "\(currentTimeMillis)\(API_PRIVATE_KEY)\(API_PUBLIC_KEY)".toMd5()! as String
        loader = nil
        viewControllerUsed = nil
    }
    
    
    func requestGET(methodAPI:String!, parameters: [String:AnyObject]! ,callback: @escaping handler){
        
        if Util.isConnectedToNetwork() {
            
            loader.showProgressHUDWithMessage(title: "Welcome to Ilegra Marvel, wait please..")
            
            
            
            //Parameters Array
            var listParameters = [String:AnyObject]()
            if let filters = parameters{
                listParameters = filters
                listParameters["apikey"] = API_PUBLIC_KEY as AnyObject
                listParameters["ts"] = currentTimeMillis as AnyObject
                listParameters["hash"] = hashMD5 as AnyObject
            }else{
                let defaultParams: Parameters = [
                    "apikey":API_PUBLIC_KEY,
                    "ts":currentTimeMillis,
                    "hash":hashMD5
                ]
                
                listParameters = defaultParams as [String : AnyObject]
            }
            
            
            Alamofire.request(URL_API_Marvel + methodAPI, method: .get, parameters: listParameters,encoding: URLEncoding(destination: .queryString), headers: nil).responseJSON(completionHandler: { (response : DataResponse<Any>) in
                callback(response,true)
            })
            
        }else{
            Alert.show(titulo: "Internet", mensagem: "Verifique sua conexão com a internet!", viewController: viewControllerUsed)
            callback(["Message":"Sem Conexão"], false)
        }
        
    }
    
    
}
