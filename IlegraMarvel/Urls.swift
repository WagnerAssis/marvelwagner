//
//  CharacterMarvel.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/19/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import ObjectMapper

class Urls: Mappable{
    var type : String?
    var url : String?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        url <- map["url"]
        
    }
}
