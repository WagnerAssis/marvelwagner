//
//  ResponseMarvel.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/19/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseMarvel: Mappable {
    var code : Int?
    var status : String?
    var copyright : String?
    var data : DataResponseMarvel?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        status <- map["status"]
        copyright <- map["copyright"]
        data <- map["data"]
    }
}
