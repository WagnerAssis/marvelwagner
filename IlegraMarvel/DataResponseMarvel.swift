//
//  ResponseMarvel.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/19/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import ObjectMapper

class DataResponseMarvel: Mappable {
    var total : Int?
    var count : Int?
    var results : [CharacterMarvel]?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        total <- map["total"]
        count <- map["count"]
        results <- map["results"]
    }
}
