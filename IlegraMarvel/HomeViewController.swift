//
//  ViewController.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/18/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import UIKit

class HomeViewController: JFCardSelectionViewController {
    
    var marvelController:MarvelController!
    var cards: [CharacterMarvel]? {
        didSet {
            reloadData()
        }
    }
    var marvelSelected = CharacterMarvel()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        marvelController = MarvelController(main: self)
        getAllHeroesMarvel()
        setupCards()
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func setupCards(){
        backgroundImage = UIImage(named: "bg.jpg")
        dataSource = self
        delegate = self
        // `.slide` or `.fade`. Defaults to `.fade`.
        selectionAnimationStyle = .fade
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "info"{
            if let destinationVC = segue.destination as? HeroDetailViewController{
                destinationVC.marvelObject = self.marvelSelected
            }
        }
    }

    private func getAllHeroesMarvel(){
        marvelController.GETAllCharactersMarvel { (result:Any, status:Bool) in
            if status{
                if let marvelObjetc = result as? ResponseMarvel{
                    self.cards = (marvelObjetc.data?.results!)!
                    
                }
            }
        }
    }

}

extension HomeViewController: JFCardSelectionViewControllerDataSource {
    
    func numberOfCardsForCardSelectionViewController(_ cardSelectionViewController: JFCardSelectionViewController) -> Int {
        return cards?.count ?? 0
    }
    
    func cardSelectionViewController(_ cardSelectionViewController: JFCardSelectionViewController, cardForItemAtIndexPath indexPath: IndexPath) -> CardPresentable {
        let cardSelected = (cards?[indexPath.row])!
        return cardSelected
    }
    
}

extension HomeViewController: JFCardSelectionViewControllerDelegate {
    
    func cardSelectionViewController(_ cardSelectionViewController: JFCardSelectionViewController, didSelectDetailActionForCardAtIndexPath indexPath: IndexPath) {
        print("didSelectDetailActionForCardAtIndexPath")
    }
    
    func cardSelectionViewController(_ cardSelectionViewController: JFCardSelectionViewController, didSelectCardAction cardAction: CardAction, forCardAtIndexPath indexPath: IndexPath) {
        guard let card = cards?[indexPath.row] else { return }
        if let action = card.actionOne, action.title == cardAction.title {
            self.marvelSelected = cards![indexPath.row]
            self.performSegue(withIdentifier: "info", sender: self)
        }
    }
    
}

