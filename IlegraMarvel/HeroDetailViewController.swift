//
//  HeroDetailViewController.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/19/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import UIKit

class HeroDetailViewController: UIViewController {
    
    @IBOutlet weak var desc: UITextView!
    @IBOutlet weak var comics: LabelSubtitulo!
    @IBOutlet weak var stories: LabelSubtitulo!
    @IBOutlet weak var series: LabelSubtitulo!
    @IBOutlet weak var events: LabelSubtitulo!
    
    var marvelObject = CharacterMarvel()
    
    override func viewWillAppear(_ animated: Bool) {
        self.fillingDatail()
        super.viewWillAppear(true)
        
    }
    
    private func fillingDatail(){
        if marvelObject.idMarvel != 0{
            if marvelObject.description!.isEmpty{
                self.desc.text = "no description."
            }else{
                self.desc.text = marvelObject.description!
            }
            self.comics.text = "Comics: \(marvelObject.comics!.available!)"
            self.stories.text = "Stories: \(marvelObject.stories!.available!)"
            self.series.text = "Series: \(marvelObject.series!.available!)"
            self.events.text = "Events: \(marvelObject.events!.available!)"
        }else{
            Alert.show(titulo: "Ops", mensagem: "Something wrong.. try again later.", viewController: self, callback: { (status:Bool?) in
                
                self.close(UIButton())
                
            })
        }
    }
    
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true) { 
            print("close detail..")
        }
    }
}
