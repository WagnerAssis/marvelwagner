//
//  CharacterMarvel.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/19/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import ObjectMapper

class Thumbnail: Mappable{
    var path : String?
    var extensionThumb : String?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        path <- map["path"]
        extensionThumb <- map["extension"]
        
    }
}
