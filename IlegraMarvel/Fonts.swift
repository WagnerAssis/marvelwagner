//
//  Fonts.swift
//  IlegraMarvel
//
//  Created by Wagner Assis on 7/18/17.
//  Copyright © 2017 Wagner Assis. All rights reserved.
//

import Foundation
import UIKit

class Fonts {
    
    let fonteSubtitulo: UIFont = UIFont(name: "HelveticaNeue", size: 16)!
    let fonteTitulo: UIFont = UIFont(name: "HelveticaNeue-UltraLight", size: 26)!
    let fonteLoader: UIFont = UIFont(name: "HelveticaNeue-UltraLight", size: 14)!
    let fonteBotao: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 15)!
    let fonteMedia: UIFont = UIFont(name: "HelveticaNeue-Bold", size: 13)!
}
